#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/queue.h"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

   ___
  / _ \ _   _  ___ _   _  ___
 | | | | | | |/ _ \ | | |/ _ \
 | |_| | |_| |  __/ |_| |  __/
  \__\_\\__,_|\___|\__,_|\___|
          (COLAS)

Una Cola o Queue es una estructura de datos
que sigue la Filosofía FIFO del ingle
 Firs  I First Out que en españo  seri
Primero en entrar primero en salir.
Esto quiere decir que el elemento que entre
primero a la Cola sera el primero que salga
y el último que entre sera el último en sali

*/

int main(int argc, char *argv[] )
{
	//Crea una estructura cola
	Queue *myQueue=createQueue();

	//Imprime la cola, deberia estar vacia en este punto
	printQueue(myQueue);

	//Creamos 3 strings
	char *x="Hola";
	char *y="Como";
	char *z="Estas";

	//Agregamos strings a la cola
	pushToQueue( myQueue, x );
	pushToQueue( myQueue, y );
	pushToQueue( myQueue, z );

	//Imprimimos cola
	printQueue(myQueue);

	//Sacamos de la cola e intentamos liberar la memoria
	char *tmp=popFromQueue(myQueue);
	tryFree(tmp);

	//Volvemos a imprimir para ver resultado
	printQueue(myQueue);

	//Repetimos el proceso hasta dejar la lista vacia
	tmp=popFromQueue(myQueue);
	tryFree(tmp);
	printQueue(myQueue);

	tmp=popFromQueue(myQueue);
	tryFree(tmp);
	printQueue(myQueue);

	tryFree(myQueue);

	//Sin errores
	return 0;
}
