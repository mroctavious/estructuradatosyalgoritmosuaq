#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/stack.h"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

     _______.___________.    ___       ______  __  ___
    /       |           |   /   \     /      ||  |/  /
   |   (----`---|  |----`  /  ^  \   |  ,----'|  '  /
    \   \       |  |      /  /_\  \  |  |     |    <
.----)   |      |  |     /  _____  \ |  `----.|  .  \
|_______/       |__|    /__/     \__\ \______||__|\__\
                        (PILAS)

Este programa es en esencia una pila, un poco mas complicado
ya que se uso la tecnica de lista doblemente ligada.
Una lista ligada es una lista enlazada de nodos, donde cada
nodo tiene un único campo de enlace. Una variable d
referencia contiene una referencia al primer nodo, cada nodo
(excepto el últimoenlaza con el nodo siguiente, y el enlace
del último nodo contiene NULL para indicar el final de la lista.

Ejemplo:

[NODO X] --> [NODO Y] --> [NODO W] --> NULL
Tamano de la lista: 3
*/


//Programa principal
int main(int argc, char *argv[] )
{
	int i;

	//Pila, se crea el nodo principal de la estructura pila(Doblemente ligada)
	Stack *laPila=createStack();

	//Llenamos con valores, se hace push
	//PUSH es agrega a la pila
	pushToStack(laPila, "FIF UAQ");
	pushToStack(laPila, "Hello");
	pushToStack(laPila, "World");

	//Empezamos a manipular el PULL
	char *elPull=pullFromStack(laPila);

	//Imprimimos elemento
	printf( "1er Pull: %s\n", elPull );

	//Intentamos liberar memoria si es posible
	tryFree(elPull);

	//Imprimimos Pila actualizada, sin el ultimo elemento popeado
	printPila(laPila);


	//Repetimos el proceso hasta dejar la lista vacia
	elPull=pullFromStack(laPila);
	printf( "2do Pull: %s\n", elPull );
	tryFree(elPull);
	printPila(laPila);

	elPull=pullFromStack(laPila);
	printf( "3ro Pull: %s\n", elPull );
	tryFree(elPull);

	elPull=pullFromStack(laPila);
	printPila(laPila);
	printf( "4to Pull: %s\n", elPull );
	tryFree(elPull);

	//Liberar la pila
	tryFree(laPila);

	//Termino sin errores
	return 0;
}
