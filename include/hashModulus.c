/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ
*/

//Reserva memoria y inicia una colision. Una colision ocurre cuando la suma
//del valor ASCII de todos los caracteres del String, aplicando el modulo M al resultado
//nos dara valores entre 0 y M-1 (que puede ser el tamano del arreglo). Si da una posicion
//que ya ocupada por otro nodo, entonces hay una colision, por lo tanto se va haciendo
//una lista ligada en esa posicion del arreglo
Collision createCollision()
{
	Collision newCol = malloc( sizeof(struct _Collision) );
	newCol->start = NULL;
	newCol->end = NULL;
	return newCol;
}

//Funcion que crea tabla de hash, solo hay que ingresarle el modulo,
//tambien puede ser el tamano de la lista y de ahi se pueden ir
//aumentando los nodos
Hash *createHash( int modulus )
{
	//Reservamos memoria para el hash completo
	Hash *newHash = malloc( sizeof(Hash) );

	//Se crea arreglo de colisiones
	newHash->Keys = malloc( sizeof(Collision) * modulus );

	//Asignamos toda la memoria que apunte a nulo, ya que se acaba de crear
	//y no hay datos y no queremos basura
	int i;
	for( i=0; i<modulus; i++ )
	{
		newHash->Keys[i]=NULL;
	}

	//Asignamos el modulo
	newHash->modulus=modulus;

	//Regresamos la estructura hash creada
	return newHash;
}

//Crear nodo de la tabla hash
HashNode *createHashNode( char *key, void *value, char type )
{
	//Reservamos memoria para el nodo
	HashNode *newNode = malloc( sizeof(HashNode) );

	//Asignamos el tipo de dato
	newNode->Type = type;


	//Independientemente del tipo de dato, es necesario
	//tenemos que copiar el string
	strcpy(newNode->Key, key);

	//Verificamos si son apuntadores
	if( type == 'I' || type == 'F' || type == 'D' || type == 'S' )
	{
		//Integer Pointer
		if( type == 'I' )
		{
			newNode->Value.IntegerPtr = malloc( sizeof(int) );
			memcpy( newNode->Value.IntegerPtr, (int*)value, sizeof(int)  );
		}
		//Float Pointer
		else if( type == 'F' )
		{
			newNode->Value.FloatPtr = malloc( sizeof(float) );
			memcpy( newNode->Value.FloatPtr, (float*)value, sizeof(float)  );
		}
		//Double Pointer
		else if( type == 'D' )
		{
			newNode->Value.DoublePtr = malloc( sizeof(double) );
			memcpy( newNode->Value.DoublePtr, (double*)value, sizeof(double)  );
		}
		//String Pointer
		else if( type == 'S' )
		{
			newNode->Value.String = malloc( sizeof(char) * strlen( (char*) value ) + 1 );
			strcpy( newNode->Value.String, (char*)value  );
		}
	}
	else
	{
		if( type == 'i' )
		{
			//Casteamos(Convertimos) el apuntador a vacio y le damos
			//forma de apuntador entero, ya que tipo es entero
			newNode->Value.Integer=*((int*)value);
		}
		else if( type == 'f' )
		{
			//Casteamos(Convertimos) el apuntador a vacio y le damos
			//forma de apuntador flotante, ya que tipo es flotante
			newNode->Value.Float=*((float*)value);
		}
		else if( type == 'd' )
		{
			//Casteamos(Convertimos) el apuntador a vacio y le damos
			//forma de apuntador double, ya que tipo es Double
			newNode->Value.Double=*((double*)value);
		}
		else if( type == 'c' )
		{
			//Casteamos(Convertimos) el apuntador a vacio y le damos
			//forma de apuntador caracter, ya que tipo es char
			newNode->Value.Char=*((char*)value);
		}

	}

	//El siguiente nodo apunta a nulo, para asi poder detenernos al momento 
	//de buscar la llave, para que no se salga del "indice"(memoria no contigua)
	newNode->next=NULL;

	//Asignamos los valores
	return newNode;
}

//Conseguimos el valor entero de cada caracter y lo sumamos,
//este numero resultante le aplicaremos el modulo tamano del arreglo para saber exactamente
//en que posicion podria estar nuestra llave
int stringValue( char *str )
{
	int i=0;
	char *tmp=str;
	while( *tmp != '\0' )
	{
		//Sumamos valor del caracter
		i+=*tmp;

		//Debug
		//printf("Letra: %c \tValor: %d \tTotal:%d\n", *tmp, *tmp, i);

		//Siguiente caracter
		++tmp;
	}
	return i;
}

//Funcion que nos devuelve el indice de la tabla hash, este indice se consigue
//sumando los valores ASCII de cada caracter del string
/*
	Letra: H 	Valor: 72 	Total:72
	Letra: o 	Valor: 111 	Total:183
	Letra: l 	Valor: 108 	Total:291
	Letra: a 	Valor: 97 	Total:388
	Letra: ! 	Valor: 33 	Total:421

Despues se le aplica un modulo al total, el modulo es el tamano
de elementos en la tabla hash, este se define al momento de que se crea.

mod=30
421%30 = 1

0   1  2  3	29
[] [] [] [] ... []  <-- Before
[] [*][] [] ... []  <-- After

Entonces se  guardara el dato en la posicion 1
*/
int hashModulusIndex( char *str, int modulus )
{
	return stringValue(str) % modulus;
}



//Busca la llave en la lista ligada, en caso de encontrarlo
//retorna la direccion de dicho nodo
HashNode *findKey( Collision startColl, char *key )
{
	//Apuntador temporal para no modificar el original
	HashNode *tmp = startColl->start;

	//Recorremos la lista ligada
	while( tmp != NULL )
	{
		//Si es la llave que buscamos, entonces retorna la direccion y fin
		if( strcmp(key, tmp->Key) == 0 )
			return tmp;
		tmp = tmp->next;
	}

	//Si retorna nulo es que no encontro nada
	return NULL;
}

void getHashValue( Hash *hashRoot, char *key, HashValue *Value)
{
	//Conseguimos el indice de la llave
	int index=hashModulusIndex( key, hashRoot->modulus );

	//Primer filtro, vemos si hay colisiones en la posicion
	if( hashRoot->Keys[index] == NULL )
		Value->Value=NULL;
	else
	{
		//Buscamos en la lista ligada
		HashNode *found = findKey( hashRoot->Keys[index], key );

		//Si no hay nada, significa que no encontro el elemento
		if( found == NULL )
			Value->Value=NULL;
		else
		{
			Value->Type=found->Type;
			Value->Value=&(found->Value);
		}

	}
}


//Checa si existe una llave
int ifExists( Hash *hashRoot, char *key )
{
	//Conseguimos el indice de la llave
	int index=hashModulusIndex( key, hashRoot->modulus );

	//Primer filtro, vemos si hay colisiones en la posicion
	if( hashRoot->Keys[index] == NULL )
		return 0;
	else
	{
		//Buscamos en la lista ligada
		HashNode *found = findKey( hashRoot->Keys[index], key );

		//Si no hay nada, significa que no encontro el elemento
		if( found == NULL )
			return 0;
		else
			return 1;
	}
}


//Funcion que agrega nodo a la tabla hash
void hashAppend( Hash *hashRoot, char *key, void *value, char type )
{
	//Conseguimos el indice de la llave
	int index=hashModulusIndex( key, hashRoot->modulus );

	//Una vez conseguido el indice buscamos si existe la llave
	if( hashRoot->Keys[index] == NULL )
	{
		//Este evento solo pasa 1 vez por cada posicion del arreglo hash
		Collision newCol = createCollision();
		hashRoot->Keys[index] = newCol;

		//Ahora creamos el nodo que se agregara al hash
		HashNode *node = createHashNode( key, value, type );

		//Asignamos el nodo al principio
		newCol->start = node;

		//Asignamos el nodo al final tambien :D, para asi siempre
		//agregar en el ultimo elemento siempre
		newCol->end = node;

	}
	else
	{
		//En caso de que ya exista una coalicion, entonces vamos
		//agregando nodos al final de la lista


		//Guardamos direccion del nodo anterior
		//HashNode *back=hashRoot->Keys[index]->end;

		//Buscamos si hay una llave existente, en caso de que si exista
		//entonces vamos a sobreescribir el valor
		HashNode *node = findKey( hashRoot->Keys[index], key );

		//Si la encontro, entonces sobreescribir
		if( node != NULL )
		{
			if( node->Type == 'I' || node->Type == 'F' || node->Type == 'D' || node->Type == 'S' )
			{
				free(node->Value.OtherStruct);

				//Integer Pointer
				if( type == 'I' )
				{
					node->Value.IntegerPtr = malloc( sizeof(int) );
					memcpy( node->Value.IntegerPtr, (int*)value, sizeof(int)  );
				}
				//Float Pointer
				else if( type == 'F' )
				{
					node->Value.FloatPtr = malloc( sizeof(float) );
					memcpy( node->Value.FloatPtr, (float*)value, sizeof(float)  );
				}
				//Double Pointer
				else if( type == 'D' )
				{
					node->Value.DoublePtr = malloc( sizeof(double) );
					memcpy( node->Value.DoublePtr, (double*)value, sizeof(double)  );
				}
				//String Pointer
				else if( type == 'S' )
				{
					node->Value.String = malloc( sizeof(char) * strlen( (char*) value ) + 1 );
					strcpy( node->Value.String, (char*)value  );
				}

			}
			else
			{
				if( type == 'i' )
				{
					//Casteamos(Convertimos) el apuntador a vacio y le damos
					//forma de apuntador entero, ya que tipo es entero
					node->Value.Integer=*((int*)value);
				}
				else if( type == 'f' )
				{
					//Casteamos(Convertimos) el apuntador a vacio y le damos
					//forma de apuntador flotante, ya que tipo es flotante
					node->Value.Float=*((float*)value);
				}
				else if( type == 'd' )
				{
					//Casteamos(Convertimos) el apuntador a vacio y le damos
					//forma de apuntador double, ya que tipo es Double
					node->Value.Double=*((double*)value);
				}
				else if( type == 'c' )
				{
					//Casteamos(Convertimos) el apuntador a vacio y le damos
					//forma de apuntador caracter, ya que tipo es char
					node->Value.Char=*((char*)value);
				}

			}
			node->Type=type;

		}

		//de lo contrario, entonces creamos nuevo nodo y lo agregamos al fin de la lista
		else
		{
			//Ahora creamos el nodo que se agregara al hash
			HashNode *node = createHashNode( key, value, type );

			//Conecta el ultimo nodo con el nuevo nodo
			hashRoot->Keys[index]->end->next=node;

			//En la estructura Colission se actualiza cual
			//es el ultimo nodo de la lista, esto agiliza la
			//insercion
			hashRoot->Keys[index]->end=node;

		}


	}
}


//Va a traves de todos los nodos y libera la memoria que contienen los nodos
void freeLinkedList( HashNode *node )
{
	HashNode *tmp = node;
	HashNode *next = node;

	//Recorremos lista ligada hasta que no haya mas elementos
	while( next != NULL )
	{
		//En caso de que el valor sea un apuntador, nuestra funcion add*Ptr copio los valores del apuntador y reservo con malloc
		//Por lo tanto es necesario liberar
		if( next->Type == 'I' || next->Type == 'F' || next->Type == 'D' || next->Type == 'S' )
			free( next->Value.OtherStruct );

		//Guardamos direccion del nodo anterior
		tmp=next;

		//Reasignamos nodo
		next = next->next;

		//Liberamos viejo nodo
		free(tmp);
	}

}

//Libera toda la tabla de hash
void hashFree( Hash *hash )
{
	int i;
	//Recorre la tabla
	for( i=0; i < hash->modulus; i++ )
	{
		//En el caso de que haya llave en esa posicion
		if( hash->Keys[i] != NULL )
		{
			if( hash->Keys[i]->start != NULL )
			{
				freeLinkedList( hash->Keys[i]->start );
			}
		}
		free( hash->Keys[i] );

	}
	free(hash->Keys);
	free(hash);
}

//Imprime la estructura value, esto porque se penso que la tabla seria
//de varios tipos de datos
void printHashValue( HashValue *value )
{
	char type=value->Type;
	//Verificamos si son apuntadores
	if( type == 'I' || type == 'F' || type == 'D' || type == 'S' )
	{
		//Integer Pointer
		if( type == 'I' )
			printf("%d", *(value->Value->IntegerPtr));

		//Float Pointer
		else if( type == 'F' )
			printf("%f", *(value->Value->FloatPtr));

		//Double Pointer
		else if( type == 'D' )
			printf("%f", *(value->Value->DoublePtr));

		//String Pointer
		else if( type == 'S' )
			printf("%s", value->Value->String);

	}
	else
	{
		if( type == 'i' )
			printf("%d", value->Value->Integer);

		else if( type == 'f' )
			printf("%f", value->Value->Float);

		else if( type == 'd' )
			printf("%f", value->Value->Double);

		else if( type == 'c' )
			printf("%c", value->Value->Char);

	}
	printf("\n");
}

//Imprimir lista ligada de cada collision(Usada en la funcion prinh)
void printLinkedList( HashNode *node )
{
	HashNode *tmp = node;

	while( tmp != NULL )
	{
		printf("|%s|-->", tmp->Key );
		tmp = tmp->next;
	}
	printf("[NULL]\n");
}

//Imprime todo el hash de forma ordenada
void printh( Hash *hash )
{
	int i;
	for( i=0; i < hash->modulus; ++i )
	{
		if( hash->Keys[i] == NULL )
		{
			printf("[%d] = |Empty|  \n", i);
		}
		else
		{
			printf("[%d] = ", i);
			printLinkedList(hash->Keys[i]->start);
		}
	}
}

