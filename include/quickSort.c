/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

	Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
	100% UAQ
*/

//Intercambio de valores
void swap( long long int *x, long long int *y )
{
	//Intercambio los valores
	long long int tmp=*x;
	*x=*y;
	*y=tmp;
}


//La elección del pivote determina las particiones de la lista de dato.
//Esta funcion va a mochar la lista, y ordenar los pequeenos pedazos
//comparandose con el pivote siempre, colocando los menores a la izquierda
//y los mayores a la derecha
iArray *partition( iArray *low, iArray *high )
{
	//Seleccionar pivote como ultimo elemento y conseguimos su valor int
	long long int x=high->value;

	//Empezamos a recorrer la lista desde el principio de la sublista
	iArray *i=low->back;

	//Recorremos la SUBlista, este for es un for de apuntadores, pero
	//en lugar de iterar en un arreglo de espacio contiguo, itera sobre
	//una lista ligada. Muy practico :)
	for( iArray *j = low; j != high; j = j->forth)
	{
		//Verifica si el valor del elemento actual es menor que el
		//valor de pivote
		if( j->value <= x )
		{
			//En caso de que i este apuntando vacio(se salio de la lista)
			i = (i == NULL)? low : i->forth; //De lo contrario seguir iterando en la lista

			//Intercambiar los valores
			swap( &(i->value), &(j->value) );

			//Aumentamos el contador de intercambios
			++swapsQuickSort;
		}
		//Aumentamos las iteraciones que hace el QS
		++iteracionesQuickSort;
	}
	i = (i == NULL)? low : i->forth;
	swap( &(i->value), &(high->value) );
	return i;
}



//Funcion recursiva que llamara el ordenamiento
void myQuickSort( iArray *low, iArray *high )
{
	//Sigue llamando recursivamente hasta que solo exista 
	//un elemento al momento de particionar, es es mi caso base
	if( high != NULL && low != high && low != high->forth )
	{
		//Partimos la lista
		iArray *part=partition(low, high);
		//Volvemos a llamar la funcion, por lo tanto se volvera a particionar,
		//cuando ya no se llegue a cumplir el if, que verifica que solo
		//existan mas de 1 elementos en la lista, es cuando permitira, ordenar
		//la parte derecha de la lista despues de haber ordenado la izquierda
		myQuickSort(low, part->back);

		//Cuando acaben todas las particiones de la izquierda, entonces se ejecutara.
		myQuickSort(part->forth, high);
	}
}

//Funcion que usa la funcion 'myQuickSort()' y funciona para llamar la funcion
//recursiva por primera vez
void quickSort( rootiArray *rootA )
{
	//Conseguimos el inicio
	iArray *root=rootA->start;

	//Y el fin de la lista
	iArray *last=getLast(root);

	//Ordenamos recursivamente
	myQuickSort(root, last);
}


//Conectar siguiente elemento
void connectForth( iArray *src, iArray *forth )
{
	src->forth=forth;
}

//Conectar elemento anterior
void connectBack( iArray *src, iArray *back )
{
	src->back=back;
}


//Se consigue el ultimo elemento de la lista
iArray *getLast( iArray *root )
{
	//Apuntadores temporales pare recorrer la lista ligada
	iArray *tmp=root;
	iArray *back=NULL;

	//Recorrer la lista
	while(tmp != NULL)
	{
		back=tmp;
		tmp=tmp->forth;
	}

	//Retornar el ultimo elemento encontrado
	return back;
}

//Crear nodo e indicar el padre
iArray *createNode( long long int value )
{
	iArray *node=malloc(sizeof(iArray));
	node->value=value;
	node->back=NULL;
	node->forth=NULL;
	return node;
}

//Liberar todos los nodos
void iArrayFree( iArray *root )
{
	iArray *tmp=root;
	while( tmp != NULL )
	{
		iArray *next=tmp->forth;
		free(tmp);
		tmp=next;
	}
	//free(root);
}

//Funcion que anade elemento a la lista
void add( rootiArray *rootArr, long long int value )
{
	iArray **root=&(rootArr->start);

	if( *root == NULL )
	{
		*root=createNode(value);
		rootArr->end=*root;
		printf("Agregado\n");

	}
	else
	{
		//iArray *last=getLast( *root );
		iArray *last=rootArr->end;;
		last->forth=createNode(value);
		connectBack(last->forth, last);
		rootArr->end=last->forth;
	}
}

//Imprimir lista ligada de enteros
void iArrayPrint( iArray *root )
{
	iArray *tmp=root;
	while( tmp != NULL )
	{
		printf("%lld  ", tmp->value);
		tmp=tmp->forth;
	}
	printf("\n");
}

//Conseguir el tamano de la lista
int getLength( iArray *root )
{
	iArray *tmp=root;
	int i=0;
	while( tmp != NULL )
	{
		tmp=tmp->forth;
		++i;
	}
	return i;
}

