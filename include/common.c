#include <stdlib.h>

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

Esta es una libreria comun, son procedimientos y funciones
que pueden tener acceso todas la librerias de la carpeta
*/


//Intenta liberar memoria en caso de que exista apuntador
void tryFree( void *ptr )
{
	if( ptr != NULL )
		free(ptr);
}

