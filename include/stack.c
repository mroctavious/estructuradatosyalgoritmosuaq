/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ
*/

//Reservamos memoria e inicializamos la estructura Stack
Stack *createStack()
{
	Stack *pila=malloc( sizeof(Stack) );
	pila->tope=NULL;
	pila->inicio=NULL;
	return pila;
}

//Crear nodo nuevo
stackNode *createStackNode( char *value, stackNode *padre )
{
	stackNode *nodo=malloc( sizeof(stackNode) );
	nodo->value=malloc( sizeof(char) * strlen(value) + 1 );
	strcpy( nodo->value, value );

	nodo->back=padre;
	nodo->next=NULL;
	return nodo;
}

//Funcion que imprime todos los elementos de la pila
void printPila( Stack *miPila )
{
	//En caso de que la lista este vacia
	if( miPila->inicio == NULL )
	{
		printf("Lista vacia \n");
		return;
	}

	//Conseguimos el nodo de inicio
	stackNode *actual=miPila->inicio;

	//Imprimir valores y continuar con el siguiente nodo
	printf("Contenido de pila:\n");
	while ( actual != NULL )
	{
		printf("\t%s\n", actual->value);
		actual=actual->next;
	}
}

//Funcion que agrega nodos a la lista doble mente ligada
void pushToStack( Stack *miPila, char *dato )
{

	//En caso de que la lista este vacia
	if( miPila->tope == NULL )
	{
		//Se crea nuevo nodo
		stackNode *nuevoNode=createStackNode(dato, NULL);

		//Asignamos por primera vez los valores de Tope(Es el ultimo elemento ingresado)
		miPila->tope=nuevoNode;

		//Inicio es donde empieza el primer apuntador de la lista
		miPila->inicio=nuevoNode;
	}
	else
	{
		//En caso de que no este vacia la lista, se crea nodo nuevo y se conecta con el nodo tope
		stackNode *nuevoNode=createStackNode(dato, miPila->tope );

		//Conectando con el nodo tope
		miPila->tope->next=nuevoNode;

		//Asignar el tope al nuevo nodo creado
		miPila->tope=nuevoNode;
	}
}

char *pullFromStack( Stack *miPila )
{
	//Caso extremo, lista vacia
	if( miPila->tope == NULL )
	{
		return NULL;
	}
	else
	{

		//Recuperar direccion donde se encuentra el valor
		char *out=miPila->tope->value;

		//Conseguimos quien es el nodo Padre
		stackNode *padre=miPila->tope->back;

		//Liberamos el nodo que se va a pullear
		free(miPila->tope);

		//En caso de que despues del pull, la lista se encuentre vacia
		if( padre == NULL )
		{
			//Reasignar direccion del tope e inicio a NULL
			miPila->tope=NULL;
			miPila->inicio=NULL;
		}
		else
		{
			//Reasignar direccion del tope
			miPila->tope=padre;
			padre->next=NULL;
		}

		//Regresar resultado del nodo eliminado
		return out;
	}
}
