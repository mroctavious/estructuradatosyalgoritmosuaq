#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.c"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ


   ___
  / _ \ _   _  ___ _   _  ___
 | | | | | | |/ _ \ | | |/ _ \
 | |_| | |_| |  __/ |_| |  __/
  \__\_\\__,_|\___|\__,_|\___|
	  (COLAS)

Una Cola o Queue es una estructura de datos
que sigue la Filosofía FIFO del ingle
 Firs  I First Out que en españo  seri
Primero en entrar primero en salir.
Esto quiere decir que el elemento que entre
primero a la Cola sera el primero que salga
y el último que entre sera el último en salir.

*/

//Estructura que guarda un String y la direccion
//del siguiente nodo(LinkedList(Lista Ligada))
typedef struct queue
{
	char *value;
	struct queue *next;
} queue;

//Esta estructura es la que va a tener el principio
//y fin de la lista ligada
typedef struct Queue
{
	struct queue *head;
	struct queue *tail;
}Queue;

//Crear nodo de cola, recibe el valor
queue *createQueueNode( char *value );

//Crear estructura cola
Queue *createQueue();

//Meter elemento a la cola
void pushToQueue( Queue *list, char *str );

//Sacar elemento de la cola y liberar memoria
char *popFromQueue( Queue *list );

//Imprimir cola
void printQueue( Queue *myQueue );

//Incluimos el cuerpo del programa
#include "queue.c"
