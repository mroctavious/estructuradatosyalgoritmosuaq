#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

	Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
	100% UAQ
   ___        _      _                   _
  / _ \ _   _(_) ___| | _____  ___  _ __| |_
 | | | | | | | |/ __| |/ / __|/ _ \| '__| __|
 | |_| | |_| | | (__|   <\__ \ (_) | |  | |_
  \__\_\\__,_|_|\___|_|\_\___/\___/|_|   \__|
		(QuickSort)

Quicksort, como Mergesort, esta basado en el paradigma dividir y vencer(conquistar).
Pasos:
	Dividir: El arreglo se particiona en dos sub-arreglos no vacíos
		 tal que cada elemento de un sub-arreglo sea menor o igual
		 a los elementos del otro sub-arreglo.
	Conquistar: Los dos arreglos son ordenados llamando recursivamente
		    a Quicksort.
	Combinar: Como cada arreglo ya está ordenado, no se requieremucho trabajo,
		  solo hay que unir los arreglos en uno solo

Para su optimizacion se creo una lista doblemente ligada de numero enteros, si
se compara con 1M de elementos se ve mucho la diferencia

*/


//Contador de Iteraciones
int iteracionesQuickSort=0;

//Contador de intercambios
int swapsQuickSort=0;


//Estructura lista ligada de enteros, el i al principio es de intArray
typedef struct _iArray
{
	long long int value;
	struct _iArray *back;
	struct _iArray *forth;
}iArray;

//Lista ligada inicial, la raiz
typedef struct _rootiArray
{
	struct _iArray *start;
	struct _iArray *end;
}rootiArray;


//Funcion que anade elemento a la lista
void add( rootiArray *rootArr, long long int value );


//Intercambio de valores
void swap( long long int *x, long long int *y );

//La elección del pivote determina las particiones de la lista de dato.
//Esta funcion va a mochar la lista, y ordenar los pequeenos pedazos
//comparandose con el pivote siempre, colocando los menores a la izquierda
//y los mayores a la derecha
iArray *partition( iArray *low, iArray *high );



//Funcion recursiva que llamara el ordenamiento
void myQuickSort( iArray *low, iArray *high );

//Funcion que usa la funcion 'myQuickSort()' y funciona para llamar la funcion
//recursiva por primera vez
void quickSort( rootiArray *rootA );


//Conectar siguiente elemento
void connectForth( iArray *src, iArray *forth );

//Conectar elemento anterior
void connectBack( iArray *src, iArray *back );

//Se consigue el ultimo elemento de la lista
iArray *getLast( iArray *root );

//Crear nodo e indicar el padre
iArray *createNode( long long int value );

//Liberar todos los nodos
void iArrayFree( iArray *root );

//Imprimir lista ligada de enteros
void iArrayPrint( iArray *root );

//Conseguir el tamano de la lista
int getLength( iArray *root );

#include "quickSort.c"
