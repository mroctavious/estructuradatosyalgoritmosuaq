/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ
*/

queue *createQueueNode( char *value )
{
	queue *out=malloc( sizeof(queue) );
	out->value=(char *)malloc( sizeof(char) * strlen(value) + 1 );
	strcpy(out->value, value);
	out->next=NULL;
	return out;
}

Queue *createQueue()
{
	Queue *out=malloc(sizeof(Queue));
	out->tail=NULL;
	out->head=NULL;
	return out;
}

void pushToQueue( Queue *list, char *str )
{
	if( list->tail == NULL )
	{
		queue *newNode=createQueueNode(str);
		list->tail = newNode;
		list->head = newNode;
	}
	else
	{
		queue *newNode=createQueueNode(str);
		list->tail->next=newNode;
		list->tail=list->tail->next;
	}
}
char *popFromQueue( Queue *list )
{
	char *out=NULL;
	if( list->head == NULL )
	{
		printf("Lista vacia :(\n");
	}
	else
	{
		queue *tmp=list->head->next;
		out=list->head->value;
		free(list->head);
		list->head=tmp;

	}
	return out;

}

void printQueue( Queue *myQueue )
{
	int i=0;
	queue *nodo=myQueue->head;

	//Verificar en caso de que este vacio
	if( nodo == NULL )
	{
		printf("\nLista vacia\n");
		return;
	}
	else
	{
		printf("\nOrder\tValue\n");
		while( nodo != NULL )
		{
			printf("%i\t%s\n", i, nodo->value);
			nodo = nodo->next;
			i++;
		}
	}
}
