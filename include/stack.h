#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.c"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

     _______.___________.    ___       ______  __  ___
    /       |           |   /   \     /      ||  |/  /
   |   (----`---|  |----`  /  ^  \   |  ,----'|  '  /
    \   \       |  |      /  /_\  \  |  |     |    <
.----)   |      |  |     /  _____  \ |  `----.|  .  \
|_______/       |__|    /__/     \__\ \______||__|\__\
                        (PILAS)

Este programa es en esencia una pila, un poco mas complicado
ya que se uso la tecnica de lista doblemente ligada.
Una lista ligada es una lista enlazada de nodos, donde cada
nodo tiene un único campo de enlace. Una variable d
referencia contiene una referencia al primer nodo, cada nodo
(excepto el últimoenlaza con el nodo siguiente, y el enlace
del último nodo contiene NULL para indicar el final de la lista.

Ejemplo:

[NODO X] --> [NODO Y] --> [NODO W] --> NULL
Tamano de la lista: 3

*/


//Estructura nodo
typedef struct stackNode
{
	char *value;
	struct stackNode *back;
	struct stackNode *next;
}stackNode;

//Estructura para la pila
typedef struct stack
{
	struct stackNode *tope;
	struct stackNode *inicio;
}Stack;

//Inicializa el stack
//Reserva memoria para la estructura Stack, e inicializa
//tope e inicio para indicar que no esta apuntando a nada
Stack *createStack();

//Crear nodo nuevo
stackNode *createStackNode( char *value, stackNode *padre );

//Funcion que imprime todos los elementos de la pila
void printStack( Stack *miPila );

//Funcion que agrega nodos a la lista doble mente ligada
void pushToStack( Stack *miPila, char *dato );

//Conseguir valor del tope de la pila, es necesario liberar lo que se retorna
char *pullFromStack( Stack *miPila );

//Incluir el cuerpo del programa
#include "stack.c"
