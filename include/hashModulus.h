#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

  _   _    _    ____  _   _   __  __           _       _
 | | | |  / \  / ___|| | | | |  \/  | ___   __| |_   _| |_   _ ___
 | |_| | / _ \ \___ \| |_| | | |\/| |/ _ \ / _` | | | | | | | / __|
 |  _  |/ ___ \ ___) |  _  | | |  | | (_) | (_| | |_| | | |_| \__ \
 |_| |_/_/   \_\____/|_| |_| |_|  |_|\___/ \__,_|\__,_|_|\__,_|___/

		( Funciones de Hash[MODULUS] )

Consiste en tomar el residuo de la división de la clave entre el numero de
componentes del arreglo.

La función hash queda definida por la siguiente formula:
		H(K) = (K mod N) + 1
Se recomienda que N sea el numero primo inmediato inferior al numero total de
elementos.

*/

//Puede contener cualquier tipo de dato pero solo 1
//para eso sirve el union
typedef union value
{
	int Integer;
	int *IntegerPtr;
	float Float;
	float *FloatPtr;
	double Double;
	double *DoublePtr;
	char *String;
	char Char;
	void *OtherStruct;
} ValueType;

typedef struct _value
{
	char Type;
	ValueType *Value;
}HashValue;

//Estructura hash, contendra valor, llave
//y la direccion del siguiente nodo hash
typedef struct _HashNode
{
	//String llave, 128 es el tamano maximo del string
	//se puede cambiar si se desea
	char Key[128];

	//Valor de la llave
	ValueType Value;

	//Este caracter nos definira que tipo de dato es
	char Type;


	//Direccion al siguiente nodo
	struct _HashNode *next;

}HashNode;

//In case of colition we create a data structure
//which will tell us which is the last node of the list,
typedef struct _Collision
{
	HashNode *start;
	HashNode *end;
}*Collision;


//Estructura que contendra nodos hash, que dentro viene las llaves y valores
typedef struct _Hash
{
	//Aqui estaran las direcciones de la primera y ultima llave
	Collision *Keys;

	//Es necesario saber de que tamano es el arreglo, este sera nuestro modulo
	int modulus;
} Hash;


//Reserva memoria y inicia una colision. Una colision ocurre cuando la suma
//del valor ASCII de todos los caracteres del String, aplicando el modulo M al resultado
//nos dara valores entre 0 y M-1 (que puede ser el tamano del arreglo). Si da una posicion
//que ya ocupada por otro nodo, entonces hay una colision, por lo tanto se va haciendo
//una lista ligada en esa posicion del arreglo
Collision createCollision();

//Funcion que crea tabla de hash, solo hay que ingresarle el modulo,
//tambien puede ser el tamano de la lista y de ahi se pueden ir
//aumentando los nodos
Hash *createHash( int modulus );

//Crear nodo de la tabla hash
HashNode *createHashNode( char *key, void *value, char type );

//Conseguimos el valor entero de cada caracter y lo sumamos,
//este numero resultante le aplicaremos el modulo tamano del arreglo para saber exactamente
//en que posicion podria estar nuestra llave
int stringValue( char *str );

//Funcion que nos devuelve el indice de la tabla hash, este indice se consigue
//sumando los valores ASCII de cada caracter del string
/*
	Letra: H 	Valor: 72 	Total:72
	Letra: o 	Valor: 111 	Total:183
	Letra: l 	Valor: 108 	Total:291
	Letra: a 	Valor: 97 	Total:388
	Letra: ! 	Valor: 33 	Total:421

Despues se le aplica un modulo al total, el modulo es el tamano
de elementos en la tabla hash, este se define al momento de que se crea.

mod=30
421%30 = 1

0   1  2  3	29
[] [] [] [] ... []  <-- Before
[] [*][] [] ... []  <-- After

Entonces se  guardara el dato en la posicion 1
*/
int hashModulusIndex( char *str, int modulus );



//Busca la llave en la lista ligada, en caso de encontrarlo
//retorna la direccion de dicho nodo
HashNode *findKey( Collision startColl, char *key );


//Consigue el valor de la llave, busca en el arreglo la llave
//y en caso de encontrarlo lo devuelve como estructura HashValue
void getHashValue( Hash *hashRoot, char *key, HashValue *Value);


//Checa si existe una llave en la tabla hash, va a comparar en la
//lista ligada en busca la llave en cada nodo, en caso de encontrarla
//devuelve un 1 que significa que SI lo ENCONTRO, de lo contrario 0
//para decir que NO lo ENCONTRO
int ifExists( Hash *hashRoot, char *key );


//Funcion que agrega nodo a la tabla hash, Verifica si existe el elemento
//en la tabla y en caso de que ya exista uno, entonces sobreescribe los valores
void hashAppend( Hash *hashRoot, char *key, void *value, char type );


//Va a traves de todos los nodos y libera la memoria que contienen los nodos, es usada
//por la funcion hashFree. Esta funcion no se usa a nivel main
void freeLinkedList( HashNode *node );

//Libera toda la tabla de hash, Hay que usar esta funcion para liberar la estructura
void hashFree( Hash *hash );

//Imprime la estructura HashValue
void printHashValue( HashValue *value );

//Imprimir lista ligada de cada collision(Usada en la funcion prinh)
void printLinkedList( HashNode *node );

//Imprime todo el hash de forma ordenada y simulando la tabla
void printh( Hash *hash );

//Incluir cuerpo del programa
#include "hashModulus.c"
