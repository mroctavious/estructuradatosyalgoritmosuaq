#include <stdio.h>
#include <stdlib.h>

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ


  _____          _             _       _
 |  ___|_ _  ___| |_ ___  _ __(_) __ _| |
 | |_ / _` |/ __| __/ _ \| '__| |/ _` | |
 |  _| (_| | (__| || (_) | |  | | (_| | |
 |_|  \__,_|\___|\__\___/|_|  |_|\__,_|_|


En la fórmula Factorial se deben multiplicar todo
 los números enteros y positivos que hay entrel
número que aparece en la fórmula y el número
La función factorial es una fórmula matemát
representada por el signo de exclamación"!"

1! = 1 * 1 = 1
3! = 1 * 2 * 3 = 6
10! = 1 * 2 * 3 … 8 * 9 * 10 = 3.628.800

*/

//Se estara llamando a si misma hasta que se cumpla
//el caso base, que es cuando x sea menor 0
int fact( int x )
{
	//Caso recursivo
	if( x > 0 )
		return x*fact(x-1);
	//Caso Base
	else
		return 1;
}


int main(int argc, char *argv[] )
{
	//Verifica la sintaxis
	if( argc < 2 ){ printf("Error de Sintaxis\nEjemplo:\n\t%s 10\n", argv[0]); exit(1);}

	//Conseguir el factorial del numero ingresado en el argumento 1
	int q=fact(atoi(argv[1]));

	//Imprimir resultado
	printf("%d\n", q);

	//Sin errores
	return 0;
}
