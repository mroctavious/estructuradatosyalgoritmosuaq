#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "include/quickSort.h"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

   ___        _      _                   _
  / _ \ _   _(_) ___| | _____  ___  _ __| |_
 | | | | | | | |/ __| |/ / __|/ _ \| '__| __|
 | |_| | |_| | | (__|   <\__ \ (_) | |  | |_
  \__\_\\__,_|_|\___|_|\_\___/\___/|_|   \__|
                (QuickSort)

Quicksort, como Mergesort, esta basado en el paradigma dividir y vencer(conquistar).
Pasos:
        Dividir: El arreglo se particiona en dos sub-arreglos no vacíos
                 tal que cada elemento de un sub-arreglo sea menor o igual
                 a los elementos del otro sub-arreglo.
        Conquistar: Los dos arreglos son ordenados llamando recursivamente
                    a Quicksort.
        Combinar: Como cada arreglo ya está ordenado, no se requieremucho trabajo,
                  solo hay que unir los arreglos en uno solo

Para su optimizacion se creo una lista doblemente ligada de numero enteros, si
se compara con 1M de elementos se ve mucho la diferencia

*/

//Funcion que crea una lista ligada de N numeros
//Pseudo aleatorios
void createRandoms(rootiArray *root, int n)
{
	int i;
	for(i=0; i<n; i++)
	{
		//AAnadimo a la lista
		add(root, rand() % (n+1) );
	}
}



int main( int argc, char **argv )
{
	//Nueva semilla
	srand( (int) time(NULL) );

	//Creamos lista raiz
	rootiArray myRoot;
	myRoot.start=NULL;
	myRoot.end=NULL;


	//Tomamos tiempo, el procedimiento es llenar la lista de N numeros
	//Pseudo aleatorios
	clock_t begin = clock();
	createRandoms(&myRoot, atoi(argv[1]));
	clock_t end = clock();

	//Convertir a segundos
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

	//Imprimimos tiempo
	printf("Elapsed time inserting the random values: %f\n", time_spent);

	//Empezamos a tomar tiempo para el ordenamiento Quicksort
	begin = clock();

	//Le mandamos solo la raiz
	quickSort(&myRoot);
	end = clock();

	//Convertir tiempo a segundos e imprimir
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Elapsed time QUICKSORT: %f\n", time_spent);


	printf("Elementos: %d\n", getLength(myRoot.start));
	printf("Iteraciones: %d      Swaps: %d\n", iteracionesQuickSort, swapsQuickSort); //Estas 2 variables, estan dentro de la libreria quicksort.

	//Imprimimos la lista ya ordenada
	iArrayPrint(myRoot.start);

	//Liberamos memoria
	iArrayFree(myRoot.start);

	//Acabo el programa sin errores
	return 0;
}
