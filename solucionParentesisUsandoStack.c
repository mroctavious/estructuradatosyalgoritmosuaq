#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/stack.h"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ
*/

//Simulamos el tipo de dato Bool
typedef enum Bool
{
	True=1,
	False=0
} Bool;

//Verificar entrada
Bool verifyInput(char *str)
{
	//Copiar el apuntador, para que cuando
	//lo modifiquemos no alteremos el original
	char *tmp=str;

	//Recorrer las letras
	while( *tmp != '\0' )
	{
		//Si se encuentra un caracter que no es parentesis
		if( *tmp != '(' && *tmp != ')' )
			return False;
		++tmp;
	}
	return True;
}

//Funcion que verifica los parentesis, checa si estan en orden correcto
Bool areParentesisOK(Stack *pila, char *str)
{
	//Copiar el apuntador, para que cuando
	//lo modifiquemos no alteremos el original
	char *tmp=str;

	//Apuntador del char, para cuando hagamos el pull
	char *pull=NULL;

	//Reservamos espacio para el parentesis que se guardara
	//+ el espacio de fin de cadena '\0'
	char parentesis[2];
	parentesis[1]='\0';

	//Reservamos memoria para el contador de parentesis abiertos
	int countParentesis=0;

	//Recorremos el string
	while( *tmp != '\0' )
	{
		//Si se abre el parentesis, entonces hacemos push
		if( *tmp == '(' )
		{
			//Metemos a la pila el caracter '('
			parentesis[0]=*tmp;
			pushToStack(pila, parentesis);

			//Se aumenta el numero de parentesis abiertos encontrados
			++countParentesis;

			//Avanzamos a la siguiente iteracion
			++tmp;
			continue;
		}

		//En caso de que se cierre el parentesis
		if( *tmp == ')' )
		{
			//Hacemos pull de la pila
			pull=pullFromStack(pila);

			//Quitamos un parentesis abierto
			--countParentesis;

			//Si la lista esta vacia, entonces
			//terminamos de analizar el string
			if( pull == NULL )
			{
				break;
			}

			//De lo contrario liberamos memoria
			tryFree(pull);
		}
		//Siguiente caracter
		++tmp;
	}

	//Si se acabo de recorrer el String, entonces si es valido
	if( *tmp == '\0' && countParentesis == 0 )
		return True;

	//De lo contrario, los parentesis estan erroneos
	else
		return False;
}


//Programa principal
int main(int argc, char *argv[] )
{
	if( argc < 2 )
	{
		printf("Wrong input!, please only use parentesis inside ' '\nExample:\n\t%s '(()))('\n", argv[0]);
		return 1;
	}
	if( verifyInput( argv[1] ) == False )
	{
		printf("Wrong input!, please only use parentesis inside ' '\nExample:\n\t%s '(()))('\n", argv[0]);
		return 1;
	}

	//Pila, se crea el nodo principal de la estructura pila(Doblemente ligada)
	Stack *laPila=createStack();

	if( areParentesisOK(laPila, argv[1]) )
		printf("Correcto :D\n");
	else
		printf("Incorrecto :/\n");

	//Liberar la pila
	tryFree(laPila);

	return 0;
}
