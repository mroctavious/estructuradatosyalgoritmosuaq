#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "include/hashModulus.h"

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ

  _   _    _    ____  _   _   __  __           _       _
 | | | |  / \  / ___|| | | | |  \/  | ___   __| |_   _| |_   _ ___
 | |_| | / _ \ \___ \| |_| | | |\/| |/ _ \ / _` | | | | | | | / __|
 |  _  |/ ___ \ ___) |  _  | | |  | | (_) | (_| | |_| | | |_| \__ \
 |_| |_/_/   \_\____/|_| |_| |_|  |_|\___/ \__,_|\__,_|_|\__,_|___/

                ( Funciones de Hash[MODULUS] )

Consiste en tomar el residuo de la división de la clave entre el numero de
componentes del arreglo.

La función hash queda definida por la siguiente formula:
                H(K) = (K mod N) + 1
Se recomienda que N sea el numero primo inmediato inferior al numero total de
elementos.

*/


void printMenu()
{
	printf ("\n\n\n\n\n\n\n\nHash Function Modulus:\n");
	printf (" [1] Add an element or change an element in hash\n");
	printf (" [2] Check if key exists\n");
	printf (" [3] Search element in hash table and get value\n");
	printf (" [4] Imprimir tabla hash\n");
	printf (" [0] Exit\n");
	printf ("\nWhat do you want to do? >");

}



//Main pa pruebas
int main( int argc, char *argv[] )
{
	//Verificar la sintaxis
	if( argc < 2 )
	{
		printf("Error de sintaxis\nExample:\n\t%s [HashTableSize]\n\t%s 30\n[HashTableSize] is the size of the hash table, you can modify this for any value. [HashTableSize] is not the max size of elements inside, it's just for the module operation... Try it!.\n", argv[0], argv[0]);
		exit(1);
	}

	//Conseguimos el modulo desde argumentos
	int modulus = atoi(argv[1]);

	//Creamos la estructura Hash :D, solo hay que indicarle el modulo
	Hash *tablaHash=createHash(modulus);
	HashValue valor; //<-- Va a tener el valor de las llaves

	//Reservamos memoria para lal linea
	char line[100];

	//Accion del usuario
	int respuesta=-1;

	while ( respuesta != 0)
	{
		//Imprimimos el menu
		printMenu();

		//Leer linea de entrada
		fgets (line, sizeof(line), stdin);

		//Convertir entrada a numero entero
		int sscanf_result = sscanf (line, "%d", &respuesta);

		//Verifica que se haya podido transformar el string a entero
		if ( (sscanf_result == 0) | (sscanf_result == EOF) )
		{
			printf ("\n:( INGRESA UN ENTERO POSITIVO\n");
			respuesta = -1;
		}

		//Reservamos memoria para los datos
		char key[128];
		char value[128];

		//Ejecutamos accion valida del usuario
		switch ( respuesta )
		{
			case 0:
				printf (" ADIOS!\n ");
				break;
			case 1:

				//Que ingrese la llave
				printf("Ingresa la llave [KEY]>");

				//Leer linea de entrada, llave
				fgets (key, sizeof(key), stdin);

				//Leer linea de entrada para el valor
				printf("Ingresa la valor [VALUE]>");
				fgets (value, sizeof(value), stdin);

				//Quitamos los saltos de linea
				int removeNewLinePos=strlen(key);
				if( key[removeNewLinePos-1] == '\n' )
					key[removeNewLinePos-1]='\0';
				//Igual para value, remover salto de linea
				removeNewLinePos=strlen(value);
				if( value[removeNewLinePos-1] == '\n' )
					value[removeNewLinePos-1]='\0';

				//Anadir a la tabla Hash, indicando que es un String el valor
				hashAppend( tablaHash, key, (void*)value, 'S' );
				printf("Elemento agregado!\n");
				break;
			case 2:
				//Que ingrese la llave
				printf("Ingresa la llave [KEY]>");

				//Leer linea de entrada
				fgets (key, sizeof(key), stdin);

				//Quitamos los saltos de linea
				removeNewLinePos=strlen(key);
				if( key[removeNewLinePos-1] == '\n' )
					key[removeNewLinePos-1]='\0';

				//Verifica si existe la llave
				if( ifExists(tablaHash, key) )
					printf("Elemento existe en la tabla =>:D\n");
				else
					printf("Elemento NO EXISTE en la tabla => :(\n");

				break;
			case 3:
				//Que ingrese la llave
				printf("Ingresa la llave [KEY]>");

				//Leer linea de entrada
				fgets (key, sizeof(key), stdin);

				//Quitamos los saltos de linea
				removeNewLinePos=strlen(key);
				if( key[removeNewLinePos-1] == '\n' )
					key[removeNewLinePos-1]='\0';

				//Buscamos el valor de la llave
				getHashValue(tablaHash, key, &valor );

				//Verifica que si exista un valor en la llave
				if( valor.Value != NULL )
				{
					printf("Valor de [%s] => ", key);

					//Funcion que imprime el valor de una llave
					printHashValue(&valor);
				}
				else
					printf("Elemento NO EXISTE en la tabla => :(\n");

				break;
			case 4:
				//Imprime todo el hash
				printh(tablaHash);
				break;

			default:
				break;
		}
	}

	hashFree( tablaHash );
	return 0;
}

