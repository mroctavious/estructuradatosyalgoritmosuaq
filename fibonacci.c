#include <stdlib.h>
#include <stdio.h>

/*
Instructor: Elieth Velázquez Chávez
Estudiante: Octavio Rodriguez Garcia

        Mayo 2018
UNIVERSIDAD AUTONOMA DE QUERETARO
FACULTAD DE INFORMATICA
        100% UAQ


  _____ _ _                                _
 |  ___(_) |__   ___  _ __   __ _  ___ ___(_)
 | |_  | | '_ \ / _ \| '_ \ / _` |/ __/ __| |
 |  _| | | |_) | (_) | | | | (_| | (_| (__| |
 |_|   |_|_.__/ \___/|_| |_|\__,_|\___\___|_|


En matemáticas, la sucesión o serie de Fiboci
hace referencia a la secuencia ordenada de
numeros descrita por Leonardo de Pisa, matematico
italiano del siglo XIII:

0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,...

A cada uno de los elementos de la serie se le conoce
con el nombre de número de Fibonacci.

*/

//Funcion que se estara llamando a si misma
//hasta que se cumpla el caso base, que se cuando
//encuentre un numero que sea menor a 2
int fibonacci(int n)
{
	//CASO BASE
	if( n < 2)
	{
		return n;
	}
	else
	{
		//Se vuelve a llamar la funcion,
		//sumando los 2 numeros anteriores
		return fibonacci(n-1)+fibonacci(n-2);
	}
}


int main(int argc, char *argv[] )
{
	//Verificamos sintaxis
	if( argc < 2 ){ printf("Error de Sintaxis\nEjemplo:\n\t%s 10\n", argv[0]); exit(1);}

	//Imprimimos el resultado de fibonacci
	printf("%d\n", fibonacci(atoi(argv[1])) );

	//Sin errores
	return 0;


}


